-- res_users
UPDATE res_users SET
	password = null,
	totp_secret = null,
	oauth_access_token = null;

-- res_users_apikeys
UPDATE res_users_apikeys SET
	key = null;

-- iap_account
UPDATE iap_account SET
	account_token = null;

-- storage_backend
UPDATE storage_backend SET
	server_env_defaults = '{}'
WHERE id != 1; -- exclude Filesystem backend

-- fetchmail_server
UPDATE fetchmail_server SET
	password = null,
	server = null;
	
-- ir_mail_server
UPDATE ir_mail_server SET
	smtp_pass = null,
	smtp_host = 'smtp.fake.local';

-- datatrans_acquirer_config
UPDATE datatrans_acquirer_config SET
	datatrans_sign = null,
	datatrans_password = null;
