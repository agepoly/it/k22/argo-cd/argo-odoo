# argo-odoo

## odoo-staging static

Always present instance of odoo with (WIP) based on last odoo backup, ~1/hour

# Staging concepts

## Cleanup

SQL screening: (sql-security-screening.sql)
- user credentials (sessions, passwords, tokens) : res_users
- banking credentials (EDI) : storage_backend
- email (IN/OUT) : fetchmail_server, ir_mail_server
- datatrans (PSP) : datatrans_acquirer_config
- random api keys .. : res_users_apikeys, iap_account

PVC screening: (pvc-security-screening.sh)
- ebics keys, data
- sessions
- tmp